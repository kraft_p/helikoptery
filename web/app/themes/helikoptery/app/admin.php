<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

add_action('acf/render_field_settings/type=image', function($field) {
    acf_render_field_setting( $field, array(
        'label'			=> 'Default Image',
        'instructions'		=> 'Appears when creating a new post',
        'type'			=> 'image',
        'name'			=> 'default_value',
    ));
});

pll_register_string('welcomeTo', 'Zapraszamy do siedziby naszej firmy!');
pll_register_string('contactUs', 'Skontaktuj się z nami!');
pll_register_string('helicopterType', 'Marka');
pll_register_string('model', 'Model');
pll_register_string('yearOfManufacture', 'Rok Produkcji');
pll_register_string('totalHours', 'Nalot');
pll_register_string('price', 'Cena');
pll_register_string('other', 'INNE');
pll_register_string('catalogNumber', 'Numer katalogowy');
