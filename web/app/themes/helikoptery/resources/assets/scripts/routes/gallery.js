import baguetteBox from 'baguettebox.js';

export default {
  init() {
    baguetteBox.run('.gallery');
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
