import baguetteBox from 'baguettebox.js';

export default {
  init() {
    if (document.querySelector('.page-template-default .gallery')) {
      baguetteBox.run('.gallery');
    }

    const header = document.querySelector('header');
    window.addEventListener('scroll', () => {
      if (document.documentElement.scrollTop > window.innerHeight && !header.classList.contains('header--small')) {
        header.classList.add('header--small');
      } else if (document.documentElement.scrollTop <= window.innerHeight && header.classList.contains('header--small')) {
        header.classList.remove('header--small');
      }
    });

    const nav = document.querySelector('nav');
    const hamburger = document.querySelector('.hamburger');
    hamburger.addEventListener('click', () => {
      if (hamburger.classList.contains('hamburger--active')) {
        hamburger.classList.remove('hamburger--active');
        nav.classList.remove('nav--open'); 
      } else {
        hamburger.classList.add('hamburger--active');
        nav.classList.add('nav--open');
      }
    });

    const helicopterGallery = document.querySelector('.helicopter .gallery');
    if (helicopterGallery) {
      const image = helicopterGallery.querySelector('.image');
      const thumbnails = helicopterGallery.querySelectorAll('.thumbnail');

      baguetteBox.run('.image');

      thumbnails.forEach((thumbnail) => {
        thumbnail.addEventListener('click', () => {
          const src = thumbnail.getAttribute('data-src');
          image.querySelector('a').href = src
          image.querySelector('img').src = src;
          
          thumbnails.forEach((t) => {
            if (t === thumbnail) {
              thumbnail.classList.add('active');
            } else {
              t.classList.remove('active');
            }
          });
        });
      });
    }
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
