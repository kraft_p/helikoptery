export default {
  init() {
    // JavaScript to be fired on the home page
    let activeCategory;
    const hash = window.location.hash;
    const gridItems = document.querySelectorAll('.grid ul li');
    const categorys = document.querySelectorAll('.categorys ul li > *');

    const gridFilter = (category) => {
      gridItems.forEach((item) => {
        if (item.classList.contains('category-' + category)) {
          item.classList.add('active');
        } else {
          item.classList.remove('active');
        }
      });
    };

    const categorysFilter = (id) => {
      categorys.forEach((category) => {
        if (category.classList.contains('button--' + id)) {
          category.classList.add('button--active');
        } else {
          category.classList.remove('button--active');
        }
      });
    };

    if (hash) {
      activeCategory = hash.substr(1);
    } else {
      activeCategory = categorys[0].getAttribute('data-category');
    }

    gridFilter(activeCategory);
    categorysFilter(activeCategory);

    categorys.forEach((categoryNode) => {
      categoryNode.addEventListener('click', () => {
        const categoryId = categoryNode.getAttribute('data-category');
        gridFilter(categoryId);
        categorysFilter(categoryId);
      });
    });
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
