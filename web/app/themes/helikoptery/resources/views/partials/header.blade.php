<header class="banner">
  <div class="container">
    <a class="brand" href="{{ get_permalink(pll_get_post(9)) }}"><img src="@asset('images/logo-wanicki.png')" /></a>

    <div class="hamburger">
      <div class="hamburger-box">
        <div class="hamburger-inner"></div>
      </div>
    </div>
    
    <nav class="nav-primary">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
      <div class="languages">
        <ul>
          <?php pll_the_languages( array( 'show_flags' => 1,'show_names' => 0 ) ); ?>
        </ul>
      </div>
    </nav>
  </div>
</header>
