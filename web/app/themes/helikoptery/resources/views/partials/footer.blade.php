<section class="idea">
  <div class="section__background" style="background-image: url('{{ the_field('company_idea_background', pll_get_post(9)) }}')"></div>

  <div class="container">
    @if (is_front_page())
    <div class="content">
      {{ the_field('company_idea', pll_get_post(9)) }}
    </div>
    @endif

    <div class="columns">
      <div class="column sale">
        {{ the_field('sale', pll_get_post(9)) }}
      </div>
      <div class="column camo">
        {{ the_field('camo', pll_get_post(9)) }}
      </div>
      <div class="column service">
        {{ the_field('service', pll_get_post(9)) }}
      </div>
    </div>

    <h3 class="welcome">@php pll_e('Zapraszamy do siedziby naszej firmy!') @endphp</h3>
  </div>
</section>

<section class="map">
  <div id="map"></div>
  <script>
    // Initialize and add the map
    var map = L.map('map').setView([51.4802014,19.2314183], 6);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    var marker = L.icon({
        iconUrl: "@asset('images/map-marker.png')",
        iconSize: [52, 72],
        popupAnchor: [0, -40],
    });

    L.marker([49.9529793,19.8888621], { icon: marker }).addTo(map).bindPopup('ul. Myślenicka 19,</br>32-031 Mogilany').openPopup();

    L.marker([52.2695858,20.9111414,15.96], { icon: marker }).addTo(map).bindPopup('<strong>Hangar</strong></br>ul. Księżycowa 3,</br>01-934 Warszawa').openPopup();
  </script>
  
</section>

<section class="contact">
  <div class="section__background" style="background-image: url('{{ the_field('footer_background', pll_get_post(9)) }}')"></div>

  <div class="container">
    <h2>Helikoptery - Wanicki</h2>

    <div class="contact-details">
      {{ the_field('contact_details', pll_get_post(9)) }}
    </div>

    <nav class="nav-secondary">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
    </nav>

    <div>Design by <a href="http://kmprojekt.pl/" target="_blank">KMProjekt.pl</a></div>
  </div>
</section>

<footer class="content-info">
  <div class="container">
    @php dynamic_sidebar('sidebar-footer') @endphp
  </div>
</footer>
