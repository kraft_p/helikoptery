{{--
  Template Name: Helicopter Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="background" style="background-image: url('{{ the_field('header_background') }}')"></div>
  <div class="container">
    <section>
      @php
        $children = get_children(array('post_parent' => pll_get_post(60)));

        $categorys = array();

        foreach ($children as $child) {
          $category = get_field('category', $child->ID);
          
          if ($category) {
            array_push($categorys, $category);
          }
        }

        $categorys = array_unique($categorys);
        $other = array_search('INNE', $categorys);

        if ($other > -1) {
          $other = array_splice($categorys, $other, 1);
          $categorys = array_merge($categorys, $other);
        }
      @endphp

      <div class="categorys">
        <ul>
          @foreach ($categorys as $category)
          <li>
            <a class="button button--{{ $category }}" href="{{ get_permalink(pll_get_post(60)) }}#{{ $category }}">@if($category === 'INNE')pll_e('INNE')@else{{ $category }}@endif</a>
          </li>
          @endforeach
        </ul>
      </div>

      <div class="helicopter">
        <div class="gallery">
          @php
            $images = get_field('gallery');
          @endphp
          <div class="image">
            <a href="{{ $images[0]['image'] }}">
              <img src="{{ $images[0]['image'] }}" />
            </a>
          </div>
          <div class="thumbnails">
            <ul>
            @foreach ($images as $image)
              <li class="thumbnail {{ $loop->index === 0 ? 'active' : '' }}" data-src="{{ $image['image'] }}" style="background-image: url('{{ $image['image'] }}')"></li>
            @endforeach
            </ul>
          </div>
        </div>

        <div class="parameters">
          <h1>{!! App::title() !!}</h1>

          @if (get_field('mark'))
          <div class="mark">
            <h3>@php pll_e('Marka') @endphp:</h3> {{ the_field('mark') }}
          </div>
          @endif
          @if (get_field('model'))
          <div class="model">
            <h3>@php pll_e('Model') @endphp:</h3> {{ the_field('model') }}
          </div>
          @endif
          @if (get_field('year'))
          <div class="year">
            <h3>@php pll_e('Rok Produkcji') @endphp:</h3> {{ the_field('year') }}
          </div>
          @endif
          @if (get_field('flight_hours'))
          <div class="flight_hours">
            <h3>@php pll_e('Nalot') @endphp:</h3> {{ the_field('flight_hours') }}
          </div>
          @endif
          @if (get_field('price'))
          <div class="price">
            <h3>@php pll_e('Cena') @endphp:</h3> {{ the_field('price') }}
          </div>
          @endif
        </div>

        @if (get_field('description'))
        <div class="description">
         {{ the_field('description') }}
        </div>
        @endif
      </div>
    </section>
    <div class="contact">
      <h2>@php pll_e('Skontaktuj się z nami!') @endphp</h2>

      @php
        $query = new WP_Query(array(
          'post_type' => 'page',
          'page_id' => pll_get_post(103)
        ));

        while ( $query->have_posts() ) : $query->the_post();
          the_content();
        endwhile;
      @endphp
      </div>
  </div>
  @endwhile
@endsection
