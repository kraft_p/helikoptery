{{--
  Template Name: Parts Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="background" style="background-image: url('{{ the_field('header_background') }}')"></div>
  <div class="container">
    <h1>{!! App::title() !!}</h1>

    <section class="sale">
      @php
        $children = get_children(array('post_parent' => pll_get_post(95)));

        $categorys = array();

        foreach ($children as $child) {
          $category = get_field('category', $child->ID);
          
          if ($category) {
            array_push($categorys, $category);
          }
        }

        $categorys = array_unique($categorys);
        $other = array_search('INNE', $categorys);

        if ($other > -1) {
          $other = array_splice($categorys, $other, 0);
          $categorys = array_merge($categorys, $other);
        }
      @endphp

      <div class="categorys">
        <ul>

          @foreach ($categorys as $category)
          <li>
            <div class="button button--{{ strtolower(remove_accents($category)) }}" data-category="{{ strtolower(remove_accents($category)) }}">@if($category === 'INNE')pll_e('INNE' )@else{{ $category }}@endif</div>
          </li>
          @endforeach
        </ul>
      </div>

      <div class="grid">
        <ul>
          @foreach ($children as $helicopter)
          @php
            $id = $helicopter->ID;
            
            $images = get_field('galeria', $id);

            $firstImage = $images[0];
            $firstImageSrc = $firstImage['image'];
            $category = get_field('category', $id);
          @endphp

          <li class="category-{{ strtolower(remove_accents($category)) }}">
            <a href="{{ get_permalink($id) }}">
              <div class="image-wrapper">
                <div class="image" style="background-image: url('{{ $firstImageSrc }}')"></div>
              </div>
            </a>
            <h2>{{ $helicopter->post_title }}</h2>
            <div class="catalog_number">
              <h3>@php pll_e('Numer katalogowy') @endphp:</h3> {{ the_field('number', $id) }}
            </div>
            <div class="price">
              <h3>@php pll_e('Cena') @endphp:</h3> {{ the_field('cena', $id) }}
            </div>
            <a href="{{ get_permalink($id) }}" class="button">@php pll_e('Sprawdź') @endphp</a>
          </li>
          @endforeach
        </ul>
      </div>
    </section>
  </div>
  @endwhile
@endsection
