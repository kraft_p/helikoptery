{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <section class="top">
    <div class="background" style="background-image: url('{{ the_field('header_background') }}')"></div>

    @if (pll_get_post(60))
    <div class="latest grid">
      <ul>
      @while (have_rows('header_gallery')) @php the_row() @endphp
        <li>
          <a href="{{ get_permalink(pll_get_post(87)) }}">
            <div class="image-wrapper">
              <div class="image" style="background-image: url('{{ get_sub_field('header_gallery_image') }}')"></div>
            </div>
          </a>
        </li>
      @endwhile
      </ul>
    </div>
    @endif
  </section>
  @endwhile
@endsection
