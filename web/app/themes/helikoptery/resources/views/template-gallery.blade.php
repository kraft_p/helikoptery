{{--
  Template Name: Gallery Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="background" style="background-image: url('{{ the_field('header_background') }}')"></div>
  <div class="container">
    <section>
      @include('partials.page-header')
      
      <div class="gallery">
        @if (have_rows('gallery'))
          @while(have_rows('gallery')) @php the_row() @endphp
          <a href="{{ the_sub_field('image') }}">
            <div style="background-image: url('{{ the_sub_field('image') }}')"></div>
          </a>
          @endwhile
        @endif
      </div>
    </section>
  </div>
  @endwhile
@endsection
