{{--
  Template Name: Contact Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <div class="background" style="background-image: url('{{ the_field('header_background') }}')"></div>
    <div class="container">
      <section class="main">
        <h1>@php pll_e('Skontaktuj się z nami!') @endphp</h1>

        @if (get_field('contact_details'))
        <div class="contact-details">
          {{ the_field('contact_details') }}
        </div>
        @endif

        @include('partials.content-page')

        @if (have_rows('text_blocks'))
          <div class="blocks">
          @while ( have_rows('text_blocks') ) @php the_row() @endphp
            <div class="block">
              {{ the_sub_field('text_block') }}
            </div>
          @endwhile
          </div>
        @endif
      </section>
    </div>
  @endwhile
@endsection
