{{--
  Template Name: Part Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="background" style="background-image: url('{{ the_field('header_background') }}')"></div>
  <div class="container">
    <section>
      @php
        $children = get_children(array('post_parent' => pll_get_post(95)));

        $categorys = array();

        foreach ($children as $child) {
          $category = get_field('category', $child->ID);
          
          if ($category) {
            array_push($categorys, $category);
          }
        }

        $categorys = array_unique($categorys);
        $other = array_search('INNE', $categorys);

        if ($other > -1) {
          $other = array_splice($categorys, $other, 1);
          $categorys = array_merge($categorys, $other);
        }
      @endphp

      <div class="categorys">
        <ul>
          @foreach ($categorys as $category)
          <li>
            <a class="button button--{{ strtolower(remove_accents($category)) }}" href="{{ get_permalink(pll_get_post(95)) }}#{{ strtolower(remove_accents($category)) }}">@if($category === 'INNE')pll_e('INNE')@else{{ $category }}@endif</a>
          </li>
          @endforeach
        </ul>
      </div>

      <div class="helicopter">
        <div class="gallery">
          @php
            $images = get_field('galeria');
          @endphp
          <div class="image">
            <a href="{{ $images[0]['image'] }}">
              <img src="{{ $images[0]['image'] }}" />
            </a>
          </div>
          <div class="thumbnails">
            <ul>
            @foreach ($images as $image)
              <li class="thumbnail {{ $loop->index === 0 ? 'active' : '' }}" data-src="{{ $image['image'] }}" style="background-image: url('{{ $image['image'] }}')"></li>
            @endforeach
            </ul>
          </div>
        </div>

        <div class="parameters">
          <h1>{!! App::title() !!}</h1>

          
          @if (get_field('number'))
          <div class="catalog_number">
            <h3>@php pll_e('Numer katalogowy') @endphp:</h3> {{ the_field('number') }}
          </div>
          @endif
          @if (get_field('cena'))
          <div class="price">
            <h3>@php pll_e('Cena') @endphp:</h3> {{ the_field('cena') }}
          </div>
          @endif
        </div>

        @if (get_field('opis'))
        <div class="description">
         {{ the_field('opis') }}
        </div>
        @endif
      </div>
    </section>
    <div class="contact">
      <h2>@php pll_e('Skontaktuj się z nami!') @endphp</h2>

      @php
        $query = new WP_Query(array(
          'post_type' => 'page',
          'page_id' => pll_get_post(103)
        ));

        while ( $query->have_posts() ) : $query->the_post();
          the_content();
        endwhile;
      @endphp
      </div>
  </div>
  @endwhile
@endsection
