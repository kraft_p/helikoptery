@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="background" style="background-image: url('{{ the_field('header_background') }}')"></div>
  <div class="container">
    @include('partials.page-header')
    @include('partials.content-page')

    @if (have_rows('gallery'))
    <div class="gallery">
      @while(have_rows('gallery')) @php the_row() @endphp
      <a href="{{ wp_get_attachment_image_src(get_sub_field('image'), 'full')[0] }}">
        @php echo wp_get_attachment_image(get_sub_field('image'), 'medium') @endphp
      </a>
      @endwhile
    </div>
    @endif
  </div>
  @endwhile
@endsection
